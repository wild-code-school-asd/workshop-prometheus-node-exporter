# workshop-prometheus-node-exporter

## Requirements

- a working instance of Prometheus on NODE01
- a working instance of Grafana on NODE01

## To-do

- [ ] Step 1: connect via ssh to your prometheus server
- [ ] Step 2: follow the step [describes here](https://prometheus.io/docs/guides/node-exporter/) to install Prometheus Node Exporter
- [ ] Step 3: Add metrics from node exporter to your Grafana's instance
- [ ] Step 4: Fill in the "Getting Started" part with instructions on the manual installation of Prometheus Node Exporter
- [ ] Step 5: Take a screenshot of your Grafana's dashboard with somes metrics from Node Exporter
- [ ] Step 6: push your project to gitlab or github and make the repository accessible to your trainer

## Getting started

@todo
